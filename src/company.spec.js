const user = require("./company");

describe("company", function() {
  it("company(3)", function() {
    expect(company(3)).toMatchSnapshot();
  });
  it("company(85)", function() {
    expect(company(85)).toMatchSnapshot();
  });
  it("throw error when string given as input", function(){
    expect(function(){
      company("string");
    }).toThrow(/company needs to be integer/);
  });
});